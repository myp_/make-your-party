import angular from 'angular';
import Header from './header/header.module';
import notificationModule from './notification/notification.module';
import MypDirectives from './directives/directives.module';


let commonModule = angular.module('app.common', [
  Header,
  MypDirectives,
  notificationModule
])
  .name;

export default commonModule;
