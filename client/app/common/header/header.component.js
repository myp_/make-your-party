import controller from './header.controller'

let headerComponent = {
  restrict: 'E',
  templateUrl: 'common/header/header.html',
  controller
};

export default headerComponent;