class HeaderController {
  constructor($state) {
    'ngInject';
    this.$state = $state;
  }

    $onInit() {
    }

  partyState() {
    return (this.$state.current.name.indexOf('party') < 0) ? false : true;
  }

  goToPartyList() {
    if (!this.partyState()) return;
    this.$state.go('partiesList', { userId: this.$state.params.userId });
  }

  goToPartyState(state) {
    this.$state.go('partiesList.party.' + state, { userId: this.$state.params.userId, partyId: this.$state.params.partyId });
  }

}

export default HeaderController;
