class NotificationService {
  constructor($uibModal) {
    'ngInject';
    this.$uibModal = $uibModal;

  }

  open(text, func) {
    return this.$uibModal.open({
      ariaLabelledBy: 'modal-title',
      ariaDescribedBy: 'modal-body',
      component: 'notification',
      backdrop: 'static',
      keyboard: false,
      size: 'sm',
      resolve: {
        text: () => {
          return text;
        },
        okButton: () => {
          return func;
        }
      }
    })
  }

}

export default NotificationService;
