import controller from './notification.controller'

let notificationComponent = {
  restrict: 'E',
  templateUrl: 'common/notification/notification.html',
  controller,
  bindings: {
    resolve: '<',
    close: '&'
  }
};

export default notificationComponent;
