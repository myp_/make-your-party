class NotificationController {
  constructor($state) {
    'ngInject';
    this.$state = $state;
  }

    $onInit() {
      this.okButton = this.resolve.okButton;
      this.text = this.resolve.text;
    }

    confirm() {
      if (this.okButton) this.okButton();
      this.close();
    }

}

export default NotificationController;
