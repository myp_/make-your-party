import angular from 'angular';
import notificationComponent from './notification.component';
import NotificationService from './notification.service';

let notificationModule = angular.module('notification', [])
    .component('notification', notificationComponent)
    .service('NotificationService', NotificationService)
    .name;

export default notificationModule;