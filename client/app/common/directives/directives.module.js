import angular from 'angular';
import { bottomBtnFixed } from './bottom-btn-fixed.directive';
import { MypAutoFocus } from './myp-autofocus.directive';

const MypDirectives = angular.module('MypDirectives', [])
	.directive('bottomBtnFixed', bottomBtnFixed)
	.directive('mypAutoFocus', MypAutoFocus)
	.name;

export default MypDirectives;