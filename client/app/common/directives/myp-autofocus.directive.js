export const MypAutoFocus = ($timeout) => {
  'ngInject';

  return {
    restrict: 'A',
    link($scope, $element, $attrs) {
      $scope.$watch($attrs.mypAutoFocus, (newValue, oldValue) => {
        if (!newValue) {
          return;
        }
        $timeout(() => $element[0].focus());
      });
    }
  }
};