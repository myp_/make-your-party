export const bottomBtnFixed = ($window) => {
    'ngInject';

    return {
        restrict: 'A',
        link($scope, $element, $ctrl) {
            setTimeout(() => {
                let windowHeight = $window.document.body.clientHeight;
                let elementTop = $element[0].offsetTop;
                if (windowHeight < elementTop) {
                 $element[0].classList.add('bottomBtnFixed');
                }
            }, 0);

            $window.addEventListener("resize", () => {
                let windowHeight = $window.document.body.clientHeight;
                let elementTop = $element[0].offsetTop;
                if (windowHeight < elementTop) {
                    $element[0].classList.add('bottomBtnFixed');
                } else {
                    $element[0].classList.remove('bottomBtnFixed');
                }
            });

        }
    }
};
