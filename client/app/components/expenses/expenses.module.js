import angular from 'angular';
import uiRouter from 'angular-ui-router';
import expensesComponent from './expenses.component';

let expensesModule = angular.module('expenses', [
  //uiRouter
])
  .component('expenses', expensesComponent)
  .name;

export default expensesModule;
