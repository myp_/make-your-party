import controller from './expenses.controller';

let expensesComponent = {
  restrict: 'E',
  templateUrl: 'components/expenses/expenses.html',
  controller,
  bindings: {
    partiesResolved: '<',
    peopleResolved: '<',
    expensesResolved: '<'
  }
};

export default expensesComponent;
