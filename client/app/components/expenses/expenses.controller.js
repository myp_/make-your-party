class ExpensesController {
  constructor(PartyService, $state, firebase, NotificationService) {
    'ngInject';
    this.partyServices = PartyService;
    this.NotificationService = NotificationService;
    this.state = $state;
    this.firebase = firebase;
  }

  $onInit() {
    this.error = 'Ви не додали жодної людини до вечірки. Будь ласка, поверніться на крок назад.';

    this.peopleChecked = [];
    this.newPrice = null;
    this.newExpense = {};
    this.showCommentVar = false;
    this.partyID = this.state.params.partyId;

    this.people = this.peopleResolved;
    angular.forEach(this.people, (item) => {
      this.peopleChecked.push(true);
    });
    this.expenses = this.expensesResolved;

    this.newExpense.people = angular.copy(this.peopleChecked);
    this.newPriceAutofocus = true;

    if (!this.people.length) {
      this.NotificationService.open('Варто почекати з витратами, поки не знаєш хто на них буде скидатися. Вернись додай кількох колєгів.', () => {
        this.state.go('partiesList.party.people', {partyId: this.partyID});
      })
    }

    setTimeout(() => {
      if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
        document.getElementById("expense-input-price").addEventListener('focusout', function(e) {
          document.getElementById("add-expense").click();
        });
      }
    }, 0);

  }

  toggleCheckedPerson(index) {
    this.newExpense.people[index] = !this.newExpense.people[index];
  }

  addExpense(event) {
    if (event && event.charCode !== 13) return;
    if (!this.newPrice) return;
    if (!this.people.length) {
      return
    }

    /**TODO Add error message when no price entered*/
    this.newExpense.price = this.newPrice;
    this.newExpense.time = Date.now();
    this.partyServices.addExpense(this.newExpense, this.expensesResolved)
      .then((ref) => {
      })
      .catch((error) => {
        console.error('Failed to add expense', error);
      });

    this.newPrice = null;
    this.newExpense = null;
    this.newExpense = {};
    this.newExpense.people = angular.copy(this.peopleChecked);
  }

  removeExpense(expenseID) {
    this.partyServices.removeExpense(expenseID, this.expensesResolved)
      .then((ref) => {
      }).catch((error) => {
      console.error('Failed to remove expense', error);
    });
  }

  updateExpense(expense) {
    expense.price = expense.price ? expense.price : 0;
    expense.comment = expense.comment ? expense.comment : '';
    this.partyServices.updateExpense(expense, this.expensesResolved)
      .then((ref) => {
      }).catch((error) => {
      console.error('Failed to update expense', error);
    });
  }

  updateToggleCheckedPerson(expense, index) {
    expense.people[index] = !expense.people[index];
    this.partyServices.updateExpense(expense, this.expensesResolved)
      .then((ref) => {
      }).catch((error) => {
      console.error('Failed to update expense', error);
    });
  }

  showComment() {
    this.showCommentVar = !this.showCommentVar;
  }

  goToSummary() {
    this.state.go('partiesList.party.summary', {userID: this.userID, partyID: this.partyID});
  }

  //Calculator
  clickCalc($event) {
    let num = $event.target.textContent;
    if (num && num.length) {
      if (this.newPrice === null || this.newPrice === undefined) {
        return this.newPrice = num;
      }
      return this.newPrice += num;
    }

  }

  removeCalc($event) {
    if (this.newPrice !== null) {
      if (this.newPrice.length > 0) {
        return this.newPrice = this.newPrice.slice(0, -1);
      }
    }
  }

  dotCalc($event) {
    if (this.newPrice !== null) {
      if (this.newPrice.length > 0) {
        return this.newPrice = this.newPrice + '.';
      }
    }
  }

  //Controller

}

export default ExpensesController;
