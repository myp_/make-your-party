class SummaryController {
  constructor(PartyService, $state) {
    'ngInject';
    this.partyServices = PartyService;
    this.state = $state;
  }

  $onInit() {
    this.party = this.partyResolved;
    this.people = this.peopleResolved;
    this.expanses = this.expensesResolved;
    this.partyID = this.state.params.partyId;

    this.calculateExpenses();
  }

  calculateExpenses() {
    let summary = 0,
      peopleSummary = {},
      people = this.clearExpenses();

    angular.forEach(this.expensesResolved, (expense, ind) => {
      summary += parseFloat(expense.price);
      var count = 0;
      angular.forEach(expense.people, (val, index) => {
        if (val) count++;
      });
      angular.forEach(expense.people, (val, index) => {
        if (val) {
          people[index].summary += parseFloat(expense.price / count);
        }
      });
    });
    angular.forEach(people, function (person) {
      person.summary = person.summary.toFixed(2);
      peopleSummary[person.$id] = person;
    });
    this.partyServices.saveExpensesForPeople(this.partiesResolved, this.partyID, peopleSummary, summary);
  }

  clearExpenses() {
    return this.people.map((person) => {
      person.summary = 0;
      return person;
    });
  }
}

export default SummaryController;
