import controller from './summary.controller';

let summaryComponent = {
  restrict: 'E',
  templateUrl: 'components/summary/summary.html',
  controller,
  bindings: {
    partyResolved: '<',
    peopleResolved: '<',
    expensesResolved: '<',
    partiesResolved: '<'
  }
}

export default summaryComponent;
