import angular from 'angular';
import partiesListComponent from './parties-list.component';

let partiesListModule = angular.module('partiesList', [])
  .component('partiesList', partiesListComponent)
  .name;

export default partiesListModule;
