import firebase from 'firebase';

class PartiesListController {
  constructor(PartyService, AuthService, $state, $rootScope, NotificationService) {
    'ngInject';
    this.party = {};
    this.showEdit = false;
    this.showMessage = false;
    this.partyService = PartyService;
    this.authService = AuthService;
    this.state = $state;
    this.$rootScope = $rootScope;
    this.NotificationService = NotificationService;

  }

  $onInit() {
    this.userParties = this.partiesResolved;
    this.authObj = this.authService.getAuthObj();

    //TODO Remove this to auth controller or route resolve
    let language = 'ua';
    this.checkAuth();
    this.translate = this.translations[language];

    if (this.userParties.length < 1) {
      let newUserParty = true;
      this.createNewParty(newUserParty);
    }
  }

  createNewParty(newUserParty) {
    let newParty = {
      creatorID: this.currentUser.uid,
      time: Date.now()
    };
    this.partyService.addParty(this.partiesResolved, newParty)
      .then((ref) => {
        let notificationMsg = newUserParty
        ? 'Привіт. Тут є сторінка з твоїми вечірками. Оскільки в тебе ще нема жодної, то ми для тебе одну створили. Тепер треба клацнути ОК і додати кілька друзів, які є з тобою. Потім переходь до витрат. Заповни на шо ви тратили гроші і хто на яку витрату скидається. Тиснеш кнопку Підсумок і маєш пораховано хто скільки винен. Гарного часу!'
        : 'Двіжинка створена. Перейти до додавання колєгів';

        this.NotificationService.open(notificationMsg, () => {
          this.state.go('partiesList.party.people', {partyId: ref.key});
        });

      })
      .catch((error) => {
        console.error('Failed to create new party', error);
      });
  }

  showEditParty(party) {
    if (party.showEdit) {
      this.updateParty(party);
    }
    party.showEdit = !party.showEdit;
  }

  updateParty(party) {
    delete party.showEdit;
    if (typeof party.name === 'undefined') return;
    this.partyService.updateParty(this.partiesResolved, party)
      .then((ref) => {
      }).catch((error) => {
      console.error('Failed update party', error);
    });
  }

  removeParty(partyID) {
    this.partyService.removeParty(this.partiesResolved, partyID)
      .then((ref) => {
      }).catch((error) => {
      console.error('Failed remove party', error);
    });
  }

  goToParty(partyID) {
    this.state.go('partiesList.party.people', {partyId: partyID});
  }

  logOut() {
    this.authObj.$signOut()
      .then(() => {
        this.state.go('auth');
      })
      .catch((err) => {
        console.error('SignOut error', err);
      });
  }

  checkAuth() {
    this.authObj.$onAuthStateChanged((firebaseUser) => {
      if (firebaseUser && typeof this.state.params.partyId === 'undefined') {
        this.state.go('partiesList', {userId: firebaseUser.uid});
      } else if (this.state.params && this.state.params.partyId && this.state.params.partyId.length) {
        return;
      } else {
        this.state.go('auth');
      }
    });
  }
}


export default PartiesListController;
