import controller from './parties-list.controller';

let partiesListComponent = {
  restrict: 'E',
  templateUrl: 'components/parties-list/parties-list.html',
  bindings: {
    partiesResolved: '<',
    currentUser: '<',
    translations: '<'
  },
  controller
};

export default partiesListComponent;
