import controller from './people.controller';

let peopleComponent = {
  restrict: 'E',
  templateUrl: 'components/people/people.html',
  controller,
  bindings: {
    partiesResolved: '<',
    peopleResolved: '<',
    expensesResolved: '<',
    translations: '<'
  }
  // require: {
  //   partyCtrl: '^^party'
  // },

};

export default peopleComponent;