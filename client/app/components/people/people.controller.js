class PeopleController {
  constructor(PartyService, $state, NotificationService) {
    'ngInject';
    this.partyServices = PartyService;
    this.NotificationService = NotificationService;
    this.state = $state;
  }

  $onInit() {
    this.newPerson = {};
    this.userID = this.state.params.userID;
    this.partyID = this.state.params.partyId;
    this.people = this.peopleResolved;
    this.isExpenses = this.expensesResolved.length ? true : false;

    let language = 'ua';
    this.translate = this.translations[language];

    setTimeout(() => {
      if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
        document.getElementById("persone-name").addEventListener('focusout', function(e) {
          document.getElementById("add-persone").click();
        });
      }
    }, 0);
  }

  addPerson() {
    if (typeof this.newPerson.name === 'undefined') {
      return false;
    }
    let oldValue = this.newPerson;
    this.newPerson = {};

    this.newPerson.time = Date.now();
    this.partyServices.addPerson(oldValue, this.peopleResolved)
      .then((ref) => {
        oldValue = {};
        if (this.isExpenses) {
          this.updateExpenseObj();
        }
        //this.addPersonField.click().focus();
      })
      .catch((error) => {
        this.newPerson = oldValue;
        console.error('Failed to add person', error);
      });
  }

  removePerson(personID, index) {
    this.partyServices.removePerson(personID, this.peopleResolved)
      .then((ref) => {
        if (this.isExpenses) {
          this.updateExpenseObjRemove(index);
        }
      }).catch((error) => {
      console.error('Failed to remove person', error);
    });
  }

  updatePerson(person) {
    this.partyServices.updatePerson(person, this.peopleResolved)
      .then((ref) => {
      }).catch((error) => {
      console.error('Failed to remove person', error);
    });
  }

  updateExpenseObj(ref) {
    let allExpenses = {};
    angular.forEach(this.expensesResolved, function (expense) {
      expense.people.push(true);
      allExpenses[expense.$id] = expense;
    });
    this.partyServices.updateAllExpenses(this.partiesResolved, this.partyID, allExpenses)
      .then((ref) => {
      }).catch((error) => {
      console.error('Failed to updateAllExpenses', error);
    });
  }

  updateExpenseObjRemove(index) {
    let allExpenses = {};
    angular.forEach(this.expensesResolved, function (expense) {
      expense.people.splice(index, 1);
      allExpenses[expense.$id] = expense;
    });
    this.partyServices.updateAllExpenses(this.partiesResolved, this.partyID, allExpenses)
      .then((ref) => {
      }).catch((error) => {
      console.error('Failed to updateAllExpenses', error);
    });
  }

  goToExpenses() {
    if (!this.people.length) {
      this.NotificationService.open('Яка то двіжинка без друзів. Додай кількох.');
      return;
    }
    this.state.go('partiesList.party.expenses', {userID: this.userID, partyID: this.partyID});
  }
}

export default PeopleController;
