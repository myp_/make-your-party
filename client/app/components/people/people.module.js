import angular from 'angular';
import uiRouter from 'angular-ui-router';
import peopleComponent from './people.component';

let peopleModule = angular.module('people', [
  //uiRouter
])
  .component('people', peopleComponent)
  .name;

export default peopleModule;
