import angular from 'angular';
import authComponent from './auth.component';

let authModule = angular.module('auth', [])
  .component('auth', authComponent)
  .name;

export default authModule;
