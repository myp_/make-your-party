class AuthController {
  constructor(PartyService, AuthService, $state) {
    'ngInject';
    this.partyService = PartyService;
    this.authService = AuthService;
    this.state = $state;
  }

  $onInit() {
    this.authPage = 'login';
    this.error = null;
    this.authObj = this.authService.getAuthObj();

    this.newUser = {};
    this.loginUser = {};
    this.checkAuth();
  }

  login() {
    // TODO Rewrite this with angularfire
    this.authService.authGoogle()
      .then((firebaseUser) => {
        this.state.go('partiesList', {userId: firebaseUser.user.uid});
      }).catch((error) => {
      console.error('Login failed: ', error);
    });
  }

  loginEmail(email, password) {
    this.authObj.$signInWithEmailAndPassword(email, password)
      .then((firebaseUser) => {
        this.state.go('partiesList', {userId: firebaseUser.uid});
        this.error = null;
      })
      .catch((error) => {
        console.error('Authentication failed:', error);
        this.error = error.message;

        switch(error.code) {
          case 'auth/user-not-found':
            this.errorLoginEmail = error.message;
            break;
          case 'auth/wrong-password':
            this.errorLoginPassword = error.message;
            break;
        }
      });
  }

  loginAnonymously() {
    this.authService.authAnonymous()
      .then((firebaseUser) => {
        this.state.go('partiesList', {userId: firebaseUser.uid});
      }).catch((error) => {
      console.error('Login failed: ', error);
    });
  }

  registerNewUser(email, password) {
    this.authObj.$createUserWithEmailAndPassword(email, password)
      .then((firebaseUser) => {
        this.loginEmail(email, password);
      })
      .catch((error) => {
        this.registerError = error.message;
        console.error('Error create new user: ', error);
      });
  }

  switchAuthPage(page) {
    this.authPage = page;
  }

  checkAuth() {
    this.authObj.$onAuthStateChanged((firebaseUser) => {
      if (firebaseUser) {
        this.state.go('partiesList', {userId: firebaseUser.uid});
      } else {
        this.state.go('auth');
      }
    });
  }

}


export default AuthController;
