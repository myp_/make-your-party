import controller from './auth.controller';

let authComponent = {
  restrict: 'E',
  templateUrl: 'components/auth/auth.html',
  bindings: {
    currentUser: '<'
  },
  controller
};

export default authComponent;
