import angular from 'angular';
import partyComponent from './party.component';

let partyModule = angular.module('party', [])
  .component('party', partyComponent)
  .name;

export default partyModule;
