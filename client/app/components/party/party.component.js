import controller from './party.controller';

let partyComponent = {
  restrict: 'E',
  templateUrl: 'components/party/party.html',
  bindings: {
    currentUser: '<',
    partiesResolved: '<',
    expensesResolved: '<',
    peopleResolved: '<'
  },
  controller
};

export default partyComponent;