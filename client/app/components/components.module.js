import angular from 'angular';
import authModule from './auth/auth.module';
import partiesListModule from './parties-list/parties-list.module';
import partyModule from './party/party.module';
import peopleModule from './people/people.module';
import expensesModule from './expenses/expenses.module';
import summaryModule from './summary/summary.module';

let componentModule = angular.module('app.components', [
  authModule,
  partiesListModule,
  partyModule,
  peopleModule,
  expensesModule,
  summaryModule
])
  .name;

export default componentModule;
