// import angular core
import angular from 'angular';

// import third party tools
import uiRouter from 'angular-ui-router';
import 'angular-ui-router/release/stateEvents';
import firebase from 'firebase';
import angularfire from 'angularfire';
import ngMessages from 'angular-messages';
import modal from 'angular-ui-bootstrap/src/modal/index-nocss.js';


// import application components
import AppComponent from './app.component';
import Components from './components/components.module';
import {services} from './shared-services/services.module';
import Common from './common/common.module';

// https://github.com/firebase/angularfire
const config = {
  apiKey: "AIzaSyB3wy4DIPsjru8kU5njNY0sTLGkI8yJEEQ",
  authDomain: "makeyourparty-59f19.firebaseapp.com",
  databaseURL: "https://makeyourparty-59f19.firebaseio.com",
  storageBucket: "makeyourparty-59f19.appspot.com",
  messagingSenderId: "965366608753"
};
firebase.initializeApp(config);

angular.module('app', [
  'firebase',
  uiRouter,
  'ui.router.state.events',
  services,
  Common,
  Components,
  'ngMessages',
  modal
])
  .config(RouteConfig)
  .component('app', AppComponent)
  .constant('FirebasePartiesRef', firebase.database().ref().child('parties'))
  .run(onAppInit);

function RouteConfig($stateProvider, $urlRouterProvider, $locationProvider) {
  "ngInject";
  if (location.hostname.indexOf('localhost') === -1) {
    $locationProvider.html5Mode(true).hashPrefix('!');
  }

  $stateProvider
    .state('auth', {
      url: '/auth',
      component: 'auth',
      resolve: {
        currentUser: function ($firebaseAuth) {
          'ngInject';
          return $firebaseAuth(firebase.auth()).$waitForSignIn();
        }
      }
    })
    .state('partiesList', {
      url: '/:userId',
      component: 'partiesList',
      resolve: {
        currentUser: function ($firebaseAuth) {
          'ngInject';
          return $firebaseAuth(firebase.auth()).$requireSignIn();
        },
        partiesResolved: function (FirebasePartiesRef, $firebaseArray, $stateParams) {
          return $firebaseArray(FirebasePartiesRef.orderByChild('creatorID').equalTo($stateParams.userId)).$loaded();
        },
        translations: function ($firebaseObject, $rootScope) {
          return $firebaseObject(firebase.database().ref().child('translate')).$loaded()
        }
      }
    })
    .state('partiesList.party', {
      url: '/party/:partyId',
      abstract: true,
      // component: 'party',
      resolve: {
        partiesResolved: function (FirebasePartiesRef, $firebaseArray) {
          return $firebaseArray(FirebasePartiesRef).$loaded();
        },
        partyResolved: function ($stateParams, $firebaseObject) {
          let partyRef = firebase.database().ref().child('parties').child($stateParams.partyId);
          return $firebaseObject(partyRef).$loaded();
        },
        peopleResolved: function ($stateParams, $firebaseArray) {
          let peopleRef = firebase.database().ref().child('parties').child($stateParams.partyId).child('people');
          return $firebaseArray(peopleRef).$loaded();
        },
        expensesResolved: function ($stateParams, $firebaseArray) {
          let expensesRef = firebase.database().ref().child('parties').child($stateParams.partyId).child('expenses');
          return $firebaseArray(expensesRef).$loaded();
        },
        currentUser: function ($firebaseAuth) {
          'ngInject';
          return $firebaseAuth(firebase.auth()).$requireSignIn();
        }
      }
    })
    .state('partiesList.party.people', {
      url: '/people',
      component: 'people',
      resolve: {
        currentUser: function ($firebaseAuth) {
          'ngInject';
          return $firebaseAuth(firebase.auth()).$requireSignIn();
        }
      }
    })
    .state('partiesList.party.expenses', {
      url: '/expenses',
      component: 'expenses',
      resolve: {
        currentUser: function ($firebaseAuth) {
          'ngInject';
          return $firebaseAuth(firebase.auth()).$requireSignIn();
        }
      }
    })
    .state('partiesList.party.summary', {
      url: '/summary',
      component: 'summary',
      resolve: {
        currentUser: function ($firebaseAuth) {
          'ngInject';
          return $firebaseAuth(firebase.auth()).$requireSignIn();
        }
      }
    });

  $urlRouterProvider
    .otherwise('/auth');
}

function onAppInit($rootScope, $state) {
  'ngInject';
  $rootScope.$on("$stateChangeError", function (event, toState, toParams, fromState, fromParams, error) {
    /** TODO this error does't catched */
    // We can catch the error thrown when the $requireSignIn promise is rejected
    // and redirect the user back to the home page
    if (error === 'AUTH_REQUIRED') {
      $state.go('auth');
    }
  });
}
