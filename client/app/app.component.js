import controller from './app.controller'

let appComponent = {
  templateUrl: 'app.html',
  restrict: 'E',
  controller
};

export default appComponent;