import angular from 'angular';
import PartyService from './party.service';
import AuthService from './auth.service';

export const services = angular.module('shared-services', ['firebase'])
  .service('PartyService', PartyService)
  .service('AuthService', AuthService)
  .name;
