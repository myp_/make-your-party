import firebase from 'firebase';

class PartyService {

  constructor($firebaseArray, $firebaseObject) {
    'ngInject';
    this.firebaseArray = $firebaseArray;
    this.firebaseObject = $firebaseObject;
  }

  /** Party methods */
  addParty(partiesResolved, newParty) {
    return partiesResolved.$add(newParty);
  }

  updateParty(partiesResolved, party) {
    let record = partiesResolved.$getRecord(party.$id);
    record.name = party.name;
    return partiesResolved.$save(record);
  }

  removeParty(partiesResolved, partyID) {
    let record = partiesResolved.$getRecord(partyID);
    return partiesResolved.$remove(record);
  }

  /** People methods */
  addPerson(person, peopleResolved) {
    return peopleResolved.$add(person);
  }

  removePerson(personID, peopleResolved) {
    let record = peopleResolved.$getRecord(personID);
    return peopleResolved.$remove(record);
  }

  updatePerson(person, peopleResolved) {
    let record = peopleResolved.$getRecord(person.$id);
    record.name = person.name;
    return peopleResolved.$save(record);
  }

  /** Expenses methods */
  addExpense(expense, expensesResolved) {
    return expensesResolved.$add(expense);
  }

  removeExpense(expenseID, expensesResolved) {
    let record = expensesResolved.$getRecord(expenseID);
    return expensesResolved.$remove(record);
  }

  updateExpense(expense, expensesResolved) {
    let record = expensesResolved.$getRecord(expense.$id);
    record.price = expense.price;
    record.people = expense.people;
    if (typeof expense.comment !== 'undefined') record.comment = expense.comment;
    return expensesResolved.$save(record);
  }

  updateAllExpenses(partiesResolved, partyID, expensesObj) {
    let record = partiesResolved.$getRecord(partyID);
    record.expenses = expensesObj;
    return partiesResolved.$save(record);
  }

  saveExpensesForPeople(partiesResolved, partyID, peopleObj, summary) {
    let record = partiesResolved.$getRecord(partyID);
    record.summary = summary;
    record.people = peopleObj;
    return partiesResolved.$save(record);
  }

}

export default PartyService;
