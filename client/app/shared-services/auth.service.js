import firebase from 'firebase';

class AuthService {

  constructor($firebaseAuth) {
    'ngInject';
    this.$firebaseAuth = $firebaseAuth;
  }

  getAuthObj() {
    return this.$firebaseAuth(firebase.auth());
  }

  authGoogle() {
    var provider = new firebase.auth.GoogleAuthProvider();
    return this.auth(provider);
  }

  logout() {
    return firebase.auth().signOut();
  }

  authAnonymous() {
    return firebase.auth().signInAnonymously();
  }

  getUser() {
    return firebase.auth().currentUser;
  }

  isLoggedIn() {
    let user = this.getUser();
    if (user) {
      return true;
    } else {
      return false;
    }
  }

  auth(provider) {
    return firebase.auth().signInWithPopup(provider);
  }

}

//AuthService.$inject = ['$firebaseArray'];
export default AuthService;