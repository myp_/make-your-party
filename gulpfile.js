const gulp = require('gulp');
const path = require('path');
const server = require('gulp-server-livereload');
const sass = require('gulp-sass');
const babelify = require('babelify');
const rename = require('gulp-rename');
const buffer = require('vinyl-buffer');
const inject = require('gulp-inject');
const plumber = require('gulp-plumber');
const browserify = require('browserify');
const sourcemaps = require('gulp-sourcemaps');
const source = require('vinyl-source-stream');
const templateCache = require('gulp-angular-templatecache');
const ngAnnotate = require('gulp-ng-annotate');


const IMAGES = __dirname + '/client/app/assets/images/**/*';
const FONTS = __dirname + '/client/app/assets/fonts/**/*';
const SASS = __dirname + '/client/app/**/*.s+(a|c)ss';
const HTML = [__dirname + '/client/app/**/*.html', __dirname + '/client/index.html'];
const PUBLIC = 'public/';
const CLIENT = 'client/';

gulp.task('images', function() {
    return gulp.src(IMAGES)
        .pipe(gulp.dest(PUBLIC + 'assets/images'));
});

gulp.task('fonts', function() {
    return gulp.src(FONTS)
        .pipe(gulp.dest(PUBLIC + 'assets/fonts'));
});

gulp.task('browserify', function() {
    return browserify({ entries: [CLIENT + 'app/app.module.js'], debug: true, insertGlobals: true })
        .transform(babelify, { presets: ['es2015'] })
        .bundle()
        .pipe(source('app.js'))
        .pipe(buffer())
        .pipe(ngAnnotate())
        .pipe(sourcemaps.init({ loadMaps: true }))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(PUBLIC));
});

gulp.task('partials', function() {
    return gulp.src(HTML)
        .pipe(plumber())
        .pipe(templateCache({
            module: 'app',
            standalone: false,
            moduleSystem: 'IIFE'
        }))
        .pipe(rename('main-partials.js'))
        .pipe(gulp.dest(PUBLIC));
});

gulp.task('sass', function() {
    return gulp.src( SASS )
        .pipe(plumber())
        .pipe(sourcemaps.init())
        .pipe(sass({
            outputStyle: 'compressed',
            sourceMapContents: true,
            sourceMap: true
        }).on('error', sass.logError))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest(PUBLIC));
});

gulp.task('inject', ['sass', 'browserify', 'partials'], function() {
    return gulp.src(CLIENT + 'index.html')
        .pipe(plumber())
        .pipe(inject(
            gulp.src(['app.js', 'main-partials.js', 'app.css'], { read: false, cwd: PUBLIC }), {
                relative: true,
                ignorePath: '../public',
                addRootSlash: true
            })
        )
        .pipe(gulp.dest(PUBLIC));
});

gulp.task('build', ['inject', 'images', 'fonts']);

gulp.task('serv', ['build'], function() {
  gulp.src('public')
    .pipe(server({
      livereload: true,
      open: true
    }));
});

gulp.task('default', ['serv', 'watch']);

gulp.task('watch', function() {
    gulp.watch(SASS, ['sass']);
    gulp.watch(CLIENT + 'app/**/*.js', ['browserify']);
    gulp.watch(IMAGES, ['images']);
    gulp.watch(HTML, ['partials']);
});


