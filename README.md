## Install

- npm install
- gulp

Щоб цілий час слухало чи є зміни
- gulp watch

## Basics

- Підключати картинки в tmp через /public/images/_.jpg | png
- Підключати шрифти в scss /fonts/_.ttf(а самі шрифти повинні знаходитьсь в src/client/fonts)

## License

A short snippet describing the license (MYP, etc.)